## Disclaimer

This is an unofficial widget I made on a weekend. Bitbucket had nothing to do with it and I'm making no copyright claims to their name.

## Dependencies

jQuery: tested with 1.8.3. Probably works with any version where $.ajax() returns a promise.

## How-to

Teh codez r in `dist/`

This repo is Bower-ready, but not on Bower. Last I checked, Bower only supports the git protocol.
Needs some ssh/https love first.

This markup will automatically be filled in with repository data on `$(document).ready()`

```html
<div class="bitbucket-widget" data-repo="aahmed/bitbucket-widget"></div>
```

If you add markup to the page later on, you can trigger a retrieval with

```javascript
$('.new-thing').bitbucketRepo();
```

You can pass in author and repo options if you don't like the data-repo attribute.

```javascript
$('.new-thing').bitbucketRepo({
	author : 'aahmed',
	repo : 'bitbucket-widget'
});
```

A `repo-retrieved` event will be fired on the element when it has been filled. You can listen for it with

```javascript
$('.new-thing').on('repo-retrieved', function() {
	doSomething();
});
```