/*globals jasmine:false, describe:false, it:false, expect:false, spyOn:false, runs:false, waitsFor:false */
/*globals $:false*/

describe('Bitbucket repo widget', function() {

	it('lives as a jQuery plugin', function() {
		expect($.fn.bitbucketRepo).toEqual(jasmine.any(Function));
	});

	it('returns `this` for chaining', function() {
		spyOn($, 'ajax').andReturn($.Deferred().promise());
		var div = $('<div data-repo="a/b" />');
		expect(div.bitbucketRepo()).toBe(div);
	});

	it("doesn't load on document.ready if disabled", function() {
		spyOn($.fn, 'bitbucketRepo').andCallThrough();

		$.fn.bitbucketRepo.loadOnDocumentReady = false;

		var ran = false;
		runs(function() {
			$(document).ready(function() {
				setTimeout(function() {
					expect($.fn.bitbucketRepo).not.toHaveBeenCalled();
					ran = true;
				}, 100);
			});
		});
		waitsFor(function() { return ran; });
	});

	it("calls out to bitbucket servers when called", function() {
		spyOn($, 'ajax').andReturn($.Deferred().promise());

		$('<div data-repo="a/b" />').bitbucketRepo();

		expect($.ajax.argsForCall[0][0].url).toMatch(/bitbucket.org\/.+\/a\/b/);
		expect($.ajax.argsForCall[0][0].dataType).toBe('jsonp');
	});

	it("accepts author and repo options", function() {
		spyOn($, 'ajax').andReturn($.Deferred().promise());

		$('<div/>').bitbucketRepo({
			author : 'a',
			repo : 'b'
		});

		expect($.ajax.argsForCall[0][0].url).toMatch(/bitbucket.org\/.+\/a\/b/);
		expect($.ajax.argsForCall[0][0].dataType).toBe('jsonp');
	});

	it("fires a repo-retrieved event when loaded", function() {
		spyOn($, 'ajax').andReturn($.Deferred().resolve({}).promise());
		var fired = false;
		runs(function() {
			$('<div  data-repo="a/b"/>').on('repo-retrieved', function() {
				fired = true;
				expect(fired).toBeTruthy();
			}).bitbucketRepo();
		});
		waitsFor(function() {
			return fired;
		});
	});

	it("adds repo info to the element", function() {
		var lastUpdated = new Date();
		var mockRepoJSON = {
			last_updated : lastUpdated.toISOString(),
			forks_count: 1,
			followers_count: 2,
			website : 'url',
			logo : 'image',
			description : 'description text'
		};

		spyOn($, 'ajax').andReturn($.Deferred().resolve(mockRepoJSON).promise());

		var div = $('<div  data-repo="a/b"/>');

		div.on('repo-retrieved', function() {
			expect(div.find('.repo-forks').text()).toBe('1');
			expect(div.has('.repo-forks[title*="fork."]')).toBeTruthy();
			expect(div.find('.repo-followers').text()).toBe('2');
			expect(div.has('.repo-followers[title*="followers."]')).toBeTruthy();
			expect(div.text()).toContain('description text');
			expect(div.has('img[src="image"]')).toBeTruthy();
			expect(div.has('a[href="url"]')).toBeTruthy();
		});
		div.bitbucketRepo();
	});
});